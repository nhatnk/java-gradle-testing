package com.codegym;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @org.junit.jupiter.api.Test
    void add1And1() {
        Calculator calculator = new Calculator();

        int result = calculator.add(1, 1);
        assertEquals(result, 2);
    }

    @org.junit.jupiter.api.Test
    void add0And0() {
        Calculator calculator = new Calculator();

        int result = calculator.add(0, 0);
        assertEquals(result, 0);
    }

    @org.junit.jupiter.api.Test
    void addNegative1And1() {
        Calculator calculator = new Calculator();

        int result = calculator.add(-1, 1);
        assertEquals(result, 0);
    }

    @org.junit.jupiter.api.Test
    void addNegative1AndNegative1() {
        Calculator calculator = new Calculator();

        int result = calculator.add(-1, -1);
        assertEquals(result, -2);
    }

    @org.junit.jupiter.api.Test

    void addOutOfBound() {
        final Calculator calculator = new Calculator();
        assertThrows(RuntimeException.class, ()->{
            calculator.add(1, Integer.MAX_VALUE);
        });
    }
}