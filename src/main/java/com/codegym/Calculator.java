package com.codegym;

public class Calculator {
    public int add(int first, int second){
        if(first / 2 + second / 2 >= Integer.MAX_VALUE / 2){
            throw new RuntimeException("Out of bound");
        }
        return first + second;
    }
}
